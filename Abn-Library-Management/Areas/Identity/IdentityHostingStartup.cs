﻿using System;
using Abn_Library_Management.Areas.Identity.Data;
using Abn_Library_Management.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(Abn_Library_Management.Areas.Identity.IdentityHostingStartup))]
namespace Abn_Library_Management.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<Abn_Library_ManagementContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("Abn_Library_ManagementContextConnection")));

                services.AddDefaultIdentity<Abn_Library_ManagementUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<Abn_Library_ManagementContext>();
            });
        }
    }
}