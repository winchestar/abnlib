﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Abn_Library_Management.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the Abn_Library_ManagementUser class
    public class Abn_Library_ManagementUser : IdentityUser
    {
    }
}
